package com.itau.mercado.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Produto {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int codProd;
	private String nomeProd;
	private String marcaProd;
	private int qtdeProd;
	private float precoProd;
	public int getCodProd() {
		return codProd;
	}
	public void setCodProd(int codProd) {
		this.codProd = codProd;
	}
	public String getNomeProd() {
		return nomeProd;
	}
	public void setNomeProd(String nomeProd) {
		this.nomeProd = nomeProd;
	}
	public String getMarcaProd() {
		return marcaProd;
	}
	public void setMarcaProd(String marcaProd) {
		this.marcaProd = marcaProd;
	}
	public int getQtdeProd() {
		return qtdeProd;
	}
	public void setQtdeProd(int qtdeProd) {
		this.qtdeProd = qtdeProd;
	}
	public float getPrecoProd() {
		return precoProd;
	}
	public void setPrecoProd(float precoProd) {
		this.precoProd = precoProd;
	}

}
