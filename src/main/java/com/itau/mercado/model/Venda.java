package com.itau.mercado.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity    //define uma entidade (tabela da base de dados)
public class Venda {
	@Id    // informa que o campo abaixo (idVenda) sera uma chave primaria
	@GeneratedValue(strategy=GenerationType.IDENTITY)  //informa o metodo que o campo (idVenda) sera incrementado automaticamente, sem a necessidade de informar o campo na requisicao
	
	private int idVenda;
	private String dataVenda;
	private float precoVenda;
	@ManyToOne(optional=false)  //informa que havera um relacionamento entre as entidades Venda e Prodoto do tipo N-1
	private Produto produto;
	
	public int getIdVenda() {
		return idVenda;
	}
	public void setIdVenda(int idVenda) {
		this.idVenda = idVenda;
	}
	public String getDataVenda() {
		return dataVenda;
	}
	public void setDataVenda(String dataVenda) {
		this.dataVenda = dataVenda;
	}
	public float getPrecoVenda() {
		return precoVenda;
	}
	public void setPrecoVenda(float precoVenda) {
		this.precoVenda = precoVenda;
	}
	public Produto getProduto() {
		return produto;
	}
	public void setProduto(Produto produto) {
		this.produto = produto;
	}
}
