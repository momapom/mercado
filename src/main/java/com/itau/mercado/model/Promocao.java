package com.itau.mercado.model;

import java.awt.List;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Promocao {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int idPromo;
	private String dateIniPromo;
	private String dateFimPromo;
	private float descPromo;
	@ManyToMany   //informa que havera um relacionamento entre as entidades Promocao e Produto do tipo N-M.
	private Set<Produto> produtos;
	public int getIdPromo() {
		return idPromo;
	}
	public void setIdPromo(int idPromo) {
		this.idPromo = idPromo;
	}
	public String getDateIniPromo() {
		return dateIniPromo;
	}
	public void setDateIniPromo(String dateIniPromo) {
		this.dateIniPromo = dateIniPromo;
	}
	public String getDateFimPromo() {
		return dateFimPromo;
	}
	public void setDateFimPromo(String dateFimPromo) {
		this.dateFimPromo = dateFimPromo;
	}
	public float getDescPromo() {
		return descPromo;
	}
	public void setDescPromo(float descPromo) {
		this.descPromo = descPromo;
	}
	public Set<Produto> getProdutos() {
		return produtos;
	}
	public void setProdutos(Set<Produto> produtos) {
		this.produtos = produtos;
	}
}
