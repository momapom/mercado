package com.itau.mercado.repository;

import org.springframework.data.repository.CrudRepository;

import com.itau.mercado.model.Promocao;

public interface PromocaoRepository extends CrudRepository<Promocao, Integer>{

}
