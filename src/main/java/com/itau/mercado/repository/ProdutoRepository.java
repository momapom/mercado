package com.itau.mercado.repository;

import org.springframework.data.repository.CrudRepository;

import com.itau.mercado.model.Produto;

// informa que podera ser realizados as operacoes de CRUD na tabela produto
public interface ProdutoRepository extends CrudRepository<Produto, Integer>{
		
}
