package com.itau.mercado;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

//assim que for executa o "main', a aplicacao ficara escutando a porta 8080 aguardando requisicoes.
//os tipos de requisicoes, barra como que cada Json deve ser criado, esta definido no Controller

@SpringBootApplication   
public class App 
{
    public static void main( String[] args )
    {
        SpringApplication.run(App.class, args);
    	System.out.println( "Hello World!" );
    }
}
