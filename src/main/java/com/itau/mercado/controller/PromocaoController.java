package com.itau.mercado.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.itau.mercado.model.Promocao;
import com.itau.mercado.model.Venda;
import com.itau.mercado.repository.ProdutoRepository;
import com.itau.mercado.repository.PromocaoRepository;

@Controller
public class PromocaoController {
	@Autowired
	PromocaoRepository promocaoRepository;
	
	@Autowired
	ProdutoRepository produtoRepository;

	@RequestMapping(path="/promocao", method=RequestMethod.POST)
	@ResponseBody
	public Promocao cadastrarPromocao(@RequestBody Promocao promocao) {
		System.out.println(promocao.getProdutos());
		
		return promocaoRepository.save(promocao);

	}
}
