package com.itau.mercado.controller;

import java.util.Set;

import org.apache.el.stream.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.itau.mercado.model.Produto;
import com.itau.mercado.model.Venda;
import com.itau.mercado.repository.ProdutoRepository;
import com.itau.mercado.repository.VendaRepository;

@Controller
public class VendaController {
	@Autowired
	VendaRepository vendaRepository;
	
	@Autowired
	ProdutoRepository produtoRepository;
	
	@RequestMapping(path="/venda", method=RequestMethod.POST)
	@ResponseBody
	public Venda cadastrarVenda(@RequestBody Venda venda) {
		
		// atualizando o estoque
		Produto produto;
		produto = produtoRepository.findOne(venda.getProduto().getCodProd());
		produto.setQtdeProd(produto.getQtdeProd() - 1);
	    
	    vendaRepository.save(venda);
	    produtoRepository.save(produto);			
	    return venda;
	}
	
	@RequestMapping(path="/venda", method=RequestMethod.GET)
	@ResponseBody
	public Iterable<Venda> obterVendas() {
		return vendaRepository.findAll();
	}

}
