package com.itau.mercado.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.itau.mercado.model.Produto;
import com.itau.mercado.repository.ProdutoRepository;

@Controller  //informa que esta classe sera um Controller, ou seja, quando for enviado requisicoes, ela sera verificada para tentar executar o JSON
public class ProdutoController {
	
	@Autowired
	ProdutoRepository produtoRepository;
	
	@RequestMapping(path="/produto", method=RequestMethod.POST)   //metodo para inclusao de registros na tabela Produto
	@ResponseBody
	public Produto cadastrarProduto(@RequestBody Produto produto) {
		return produtoRepository.save(produto);

	}
	
	@RequestMapping(path="/produto", method=RequestMethod.GET)  //neste metodo sera retornados todos os registros da tabela Produto
	@ResponseBody
	public Iterable<Produto> buscaNomeProduto() {
		return produtoRepository.findAll();

	}
	
}